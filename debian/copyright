Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: OrthancMySQL
Upstream-Contact: Sebastien Jodogne <s.jodogne@gmail.com>
Source: https://orthanc.uclouvain.be/hg/orthanc-databases/
Files-Excluded:
 Resources/Orthanc/Sdk-*

Files: *
Copyright: 2012-2016 Sebastien Jodogne <s.jodogne@gmail.com>, University Hospital of Liege (Belgium), and 2017-2023 Osimis S.A. (Belgium), and 2021-2023 UCLouvain (Belgium)
License: AGPL-3.0+

Files: Resources/Orthanc/*
Copyright: 2012-2016 Sebastien Jodogne <s.jodogne@gmail.com>, University Hospital of Liege (Belgium), and 2017-2023 Osimis S.A. (Belgium), and 2021-2023 UCLouvain (Belgium)
License: LGPL-3.0+

Files: Resources/Orthanc/Databases/* Resources/Orthanc/Plugins/*
Copyright: 2012-2016 Sebastien Jodogne <s.jodogne@gmail.com>, University Hospital of Liege (Belgium), and 2017-2023 Osimis S.A. (Belgium), and 2021-2023 UCLouvain (Belgium)
License: GPL-3.0+




License: AGPL-3.0+
 This program is free software: you can redistribute it and/or
 modify it under the terms of the GNU Affero General Public License
 as published by the Free Software Foundation, either version 3 of
 the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Affero General Public License for more details.
 . 
 You should have received a copy of the GNU Affero General Public License
 along with this package; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


License: GPL-3.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this package; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.


License: LGPL-3.0+
 This program is free software: you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public License
 as published by the Free Software Foundation, either version 3 of
 the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-3'.
